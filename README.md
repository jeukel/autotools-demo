# Evaluación Tutorial 4. Introducción a Autotools

## Generación de Makefiles
`autoreconf --install`


## Comprobación de contrucción
```
mkdir -p build/usr
./configure --prefix=/home/"$USER"/autotools-demo/build/usr/
make
make install
```

## Generador de librería
`make distcheck`
