#ifndef MATH_H
#define MATH_H

#include <math.h>

double suma(double a, double b);
double resta(double a, double b);
double multi(double a, double b);
double div(double a, double b);
double raiz(double a);

#endif // MATH_H
